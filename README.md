# ImDiff: Compare Directories of Images

This is a simple tool to compare images of the same names under different directories. It's intended use is to validate generated images from software tests.
