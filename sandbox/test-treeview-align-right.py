import tkinter as tk
from tkinter import ttk, font

root = tk.Tk()
style = ttk.Style(root)
style.configure('Treeview', rowheight=font.Font().metrics('linespace'))

tree = ttk.Treeview(root, columns=('stat',), height=4)
tree.heading('#0', text='File')
tree.column('#0', anchor='e', width=200)
tree.heading('stat', text='Stat')
tree.column('stat', width=60, stretch=False, anchor='center')

data = (
    ('/long/long/path/to/a/file.txt', '1'),
    ('/different/long/path/to/a/file.txt', '2'),
    ('/file.txt', '3'),
)

for file_path, stat in data:
    tree.insert('', 'end', file_path, text=file_path)
    tree.set(file_path, 'stat', stat)

tree.pack()
root.mainloop()
